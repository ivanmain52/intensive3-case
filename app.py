import json

from menu import Menu
from user import User


class App:
    def __init__(self, filename):
        self.user = User()
        self.contacts = []
        self.filename = filename
        self.load()

    def load(self):
        try:
            with open(self.filename) as datafile:
                data = json.load(datafile)
        except FileNotFoundError:
            data = None

        if not data:
            return

        self.user.update(**data['user'])
        for contact_data in data['contacts']:
            contact = User()
            contact.update(**contact_data)
            self.contacts.append(contact)

    def save(self):
        data = {
            'user': self.user.to_dict(),
            'contacts': [contact.to_dict() for contact in self.contacts],
        }

        with open(self.filename, 'w') as datafile:
            json.dump(data, datafile)

    def run(self):
        print('Приложение Profile')
        print('Сохраняй информацию о себе и выводи ее в разных форматах')

        profile_menu_choices = [
            (1, 'Ввести общую информацию', self.input_general_info),
            (2, 'Ввести социальные сети и мессенджеры', self.input_social_links),
            (3, 'Вывести общую информацию', self.print_general_info),
            (4, 'Вывести всю информацию', self.print_full_info),
            (0, 'Назад', None),
        ]
        profile_menu = Menu('ПРОФИЛЬ', profile_menu_choices)

        contact_menu_choices = [
            (1, 'Посмотреть список контактов', self.print_contact_list),
            (2, 'Добавить контакт', self.add_contact),
            (3, 'Удалить контакт', self.delete_contact),
            (0, 'Назад', None),
        ]
        contact_menu = Menu('КОНТАКТЫ', contact_menu_choices)

        main_menu_choices = [
            (1, 'Профиль', profile_menu.display),
            (2, 'Контакты', contact_menu.display),
            (0, 'Завершить работу', None),
        ]
        main_menu = Menu('ГЛАВНОЕ МЕНЮ', main_menu_choices)
        main_menu.display()

    def input_general_info(self):
        # input general info
        self.user.input_general_info()
        self.save()

    def input_social_links(self):
        # input social links
        self.user.input_social_links()
        self.save()

    def print_general_info(self):
        print('ОБЩАЯ ИНФОРМАЦИЯ')
        self.user.print_info(full=False)

    def print_full_info(self):
        print('ВСЯ ИНФОРМАЦИЯ')
        self.user.print_info(full=True)

    def print_contact_list(self):
        print('СПИСОК КОНТАКТОВ')
        if not self.contacts:
            print('Список контактов пуст')
            return
        for index, contact in enumerate(self.contacts, start=1):
            print(index, contact.name)

    def add_contact(self):
        print('ДОБАВИТЬ КОНТАКТ')
        new_contact = User()
        new_contact.input_general_info()
        new_contact.input_social_links()
        self.contacts.append(new_contact)
        self.sort_contacts()
        self.save()

    def delete_contact(self):
        print('УДАЛИТЬ КОНТАКТ')
        try:
            index = int(input('Введите номер контакта в списке: '))
        except ValueError:
            index = 0
        index -= 1
        if 0 <= index < len(self.contacts):
            self.contacts.pop(index)
            self.sort_contacts()
            self.save()
            print('Контакт удален')
        else:
            print('Не существует контакта с таким номером')

    def sort_contacts(self):
        self.contacts = sorted(self.contacts, key=lambda x: x.name.lower())
