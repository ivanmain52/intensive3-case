from utils import add_years_name


class User:
    def __init__(self, name='', age=0, phone='', email='', info='',
                 vkontakte='', facebook='', instagram='', telegram='',
                 tiktok='', inn='', ogrn='', rasch_schet='', bank='',
                 bik='', corr_schet=''):
        # user profile
        self.name = name
        self.age = age
        self.phone = phone
        self.email = email
        self.info = info
        # social links
        self.vkontakte = vkontakte
        self.facebook = facebook
        self.instagram = instagram
        self.telegram = telegram
        self.tiktok = tiktok
        self.inn = inn
        self.ogrn = ogrn
        # bank
        self.rasch_schet = rasch_schet
        self.bank = bank
        self.bik = bik
        self.corr_schet = corr_schet


    def update(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)

    def to_dict(self):
        return {
            'name': self.name,
            'age': self.age,
            'phone': self.phone,
            'email': self.email,
            'info': self.info,
            # social links
            'vkontakte': self.vkontakte,
            'facebook': self.facebook,
            'instagram': self.instagram,
            'telegram': self.telegram,
            'tiktok': self.tiktok,
            'inn': self.inn,
            'ogrn': self.ogrn,
            'rasch_schet': self.rasch_schet,
            'bank': self.bank,
            'bik': self.bik,
            'corr_schet': self.corr_schet
        }

    def input_general_info(self):
        name = input('Введите имя: ')
        while True:
            # validate user age
            try:
                age = int(input('Введите возраст: '))
            except ValueError:
                age = 0
            if age > 0:
                break
            print('Возраст должен быть положительным')
        phone = input('Введите номер телефона: ')
        email = input('Введите адрес электронной почты: ')
        info = input('Введите дополнительную информацию:\n')
        inn = input('Введите ИНН:\n')
        ogrn = input('Введите ОГРН\n')
        rasch_schet = input('Введите рассчетный счет\n')
        bank = input('Банк\n')
        bik = input('Введите БИК\n')
        corr_schet = input('Введите кор счет\n')

        self.update(name=name, age=age, phone=phone, email=email, info=info,\
        inn=inn, ogrn=ogrn, rasch_schet=rasch_schet, bank=bank, bik=bik,
                    corr_schet=corr_schet)

    def input_social_links(self):
        vkontakte = input('Введите адрес профиля Вконтакте: ')
        facebook = input('Введите адрес профиля Facebook: ')
        instagram = input('Введите адрес профиля Instagram: ')
        telegram = input('Введите логин Telegram: ')
        tiktok = input('Введите логин Tiktok: ')
        self.update(vkontakte=vkontakte, facebook=facebook,
                    instagram=instagram, telegram=telegram,
                    tiktok=tiktok)

    def print_info(self, full=False):
        print('Имя:    ', self.name)
        print('Возраст:', add_years_name(self.age))
        print('Телефон:', self.phone)
        print('E-mail: ', self.email)
        print('ИНН: ', self.inn)
        print('ОГРН: ', self.ogrn)
        print('Расчетный счет: ', self.rasch_schet)
        print('Банк: ', self.bank)
        print('БИК: ', self.bik)
        print('Кор счет: ', self.corr_schet)


        if self.info:
            print('')
            print('Дополнительная информация:')
            print(self.info)

        if full:
            # print social links
            print('')
            print('Социальные сети и мессенджеры')
            print('Вконтакте:', self.vkontakte)
            print('Facebook: ', self.facebook)
            print('Instagram:', self.instagram)
            print('Telegram: ', self.telegram)
            print('Tiktok:   ', self.tiktok)
